A DSA is needed for the following source packages in old/stable. The specific
CVE IDs do not need to be listed, they can be gathered in an up-to-date manner from
https://security-tracker.debian.org/tracker/source-package/SOURCEPACKAGE
when working on an update.

Some packages are not tracked here:
- Linux kernel (tracking in kernel-sec repo)
- Embargoed issues continue to be tracked in separate file.

To pick an issue, simply add your uid behind it.

If needed, specify the release by adding a slash after the name of the source package.

--
389-ds-base (fw)
  Thorsten Alteholz proposed an update
--
ansible
  Maintainer is preparing updates
--
chromium-browser
--
glusterfs
--
gnutls28
--
libapache-mod-jk
  Maintainer proposing an update (and backportig the buster version)
--
libidn
  santiago proposed debdiffs for jessie and stretch
--
libspring-java
--
libxml2 (carnil)
  Re-evaluate situation for unstable first, risky to expose some fixes directly
--
linux
  Wait until more issues have piled up
--
mariadb-10.1/stable
  maintainer proposed to update to a version up to 10.1.34 upstream and
  including some other changes -> Needs review if suitable to include via
  security upload or need an SRM ack first.
--
mercurial
--
mkvtoolnix
--
openjpeg2 (luciano)
--
passenger
--
pdns
--
php7.0
  wait until more severe issues have come up
--
smarty3
--
sssd
  Maintainer prepared an update and proposed debdiff, acked for upload, but update needs further testing before release.
--
tiff
--
xml-security-c
--
